#ifndef CARDSTACKSMODEL_H
#define CARDSTACKSMODEL_H

#include "CardModel.h"

class Card;

/**
  CardStacksModel is the model used for multicolumns models, for example the 4 aces stacks could be stored in this model,
  but also the 7 solitaire columns of cards.
  This model is more complex than the CardListModel and it is also more difficult to use in QML because ListView, GridView
  and other view item only support QAbstractListModel subclasses.
  This model only handle one level of the tree, cards are always leafs, never children of another card.
  */
class CardStacksModel : public CardModel
{
    Q_OBJECT
public:
    explicit CardStacksModel(int columnCount, QObject *parent = 0);
    virtual ~CardStacksModel();

    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &child) const;

    // Manipulation of the model
    void pushCard(int stackIndex, Card* card);
    void pushCards(int stackIndex, QList<Card*> cards);
    Card* popCard(int stackIndex);
    QList<Card*> popLasts(int stackIndex, int popCount);
    QList<Card*> takeAll(int stackIndex);
    void clearStack(int stackIndex);
    void clearAllStacks();
    void setCardSelectable(int stackIndex, const Card* card, bool select);
    void setCardSelectable(int stackIndex, int suit, int value, bool select);
    void setCardFlipped(int stackIndex, const Card* card, bool flipped);
    void setCardFlipped(int stackIndex, int suit, int value, bool flipped);

    // Help debug
    void dumpModelData() const;

    // Model info
    int stacksCount() const { return cards.size(); }
    bool isEmpty(int stackIndex) const;
    int emptyStacksCount() const;
    int stackCardsCount(int stackIndex) const;
    bool contains(int stackIndex, int suit, int value) const;
    const Card* getCard(int stackIndex, int suit, int value) const;
    QList<const Card*> cardList(int stackIndex) const;

public slots:
    void setAllFlipped(int stackIndex, bool flipped);
    void setAllSelectable(int stackIndex, bool selectable);

private:
    QList< QList<Card*> > cards;
};

#endif // CARDSTACKSMODEL_H
