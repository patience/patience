#include "Freecell.h"

#include <QDeclarativeContext>
#include <QtDebug>
#include <QPointer>

#include "Card.h"
#include "CardStacksModel.h"
#include "CardListModel.h"

Freecell::Freecell(QObject *parent) :
    QObject(parent)
{
    freecell = new CardStacksModel(8, this);
    aces = new CardStacksModel(4, this);
    swap = new CardStacksModel(4, this);

    newGame();
}

void Freecell::newGame() {
    // Clear all cards from the game
    freecell->clearAllStacks();
    aces->clearAllStacks();
    swap->clearAllStacks();

    // Build new deck
    QPointer<CardListModel> deck(new CardListModel());
    deck->buildDeck52();

    // Shuffle the deck
    deck->shuffle();

    // Distribute cards to the solitaire
    while(!deck->isEmpty()) {
        for (int i = 0; i < freecell->stacksCount(); ++i) {
            Card* card = deck->popCard();
            if (card == 00) break;
            card->flip();
            freecell->pushCard(i, card);
        }
    }

    for (int i = 0; i < freecell->stacksCount(); ++i) {
        setFreecellColumnSelectable(i);
    }
}

//flipped == visible
void Freecell::freecellMove(int startColumn, int endColumn)
{
    Q_ASSERT(startColumn >= 0 && startColumn < freecell->stacksCount());
    Q_ASSERT(endColumn >= 0 && endColumn < freecell->stacksCount());
    qDebug() << "freecell move" << startColumn << endColumn;
    if (startColumn == endColumn) return;

    // Get cards that may moves
    QList<const Card*> movingList = movingCards(startColumn);
    if (movingList.isEmpty()) return;

    // Check if the whole is able to move
    if (!freecell->stackCardsCount(endColumn) == 0) {
        while (!movingList.isEmpty()) {
            if (isFreecellStacking(freecell->cardList(endColumn).last(), movingList.first())) break;
            else movingList.removeFirst();
        }
    }
    if (movingList.isEmpty()) return;

    // Check have enough room to move := 1 + free_swap * free_stack;
    int freeStackCount = freecell->emptyStacksCount();
    if (freecell->stackCardsCount(endColumn) == 0) freeStackCount--;
    int freeSwapCount = swap->emptyStacksCount();
    int ableToMoveCount = (1 + freeSwapCount) * (freeStackCount + 1);
    if (movingList.count() > ableToMoveCount) return;

    // Makes real move
    QList<Card*> poppedCards = freecell->popLasts(startColumn, movingList.count());
    freecell->pushCards(endColumn, poppedCards);
    setFreecellColumnSelectable(startColumn);
}

void Freecell::freecellToAces(int freecellColumn, int acesColumn)
{
    Q_ASSERT(freecellColumn >= 0 && freecellColumn < freecell->stacksCount());
    qDebug() << "freecell to aces" << freecellColumn << acesColumn;
    if (freecell->stackCardsCount(freecellColumn) == 0) return;
    const Card* lastFreecell = freecell->cardList(freecellColumn).last();

    if (acesColumn >= 0 && acesColumn < aces->stacksCount()) {
        if (cardToAces(lastFreecell, acesColumn))
            freecell->popCard(freecellColumn);
    }
    else if (acesColumn < 0) {
        for (int i = 0; i < aces->stacksCount(); i++) {
            if (cardToAces(lastFreecell, i)) {
                freecell->popCard(freecellColumn);
                break;
            }
        }
    }

    setFreecellColumnSelectable(freecellColumn);
}

void Freecell::freecellToSwap(int freecellColumn, int swapColumn)
{
    Q_ASSERT(freecellColumn >= 0 && freecellColumn < freecell->stacksCount());
    qDebug() << "freecell to swap" << freecellColumn << swapColumn;
    if (freecell->stackCardsCount(freecellColumn) == 0) return;
    if (swap->emptyStacksCount() == 0) return;

    if (swapColumn < 0) {
        for (int column = 0; column < swap->stacksCount(); column++) {
            if (swap->stackCardsCount(column) == 0) {
                swapColumn = column;
                break;
            }
        }
    }

    if (swap->stackCardsCount(swapColumn) > 0) return;

    Card* card = freecell->popCard(freecellColumn);
    swap->pushCard(swapColumn, card);

    setFreecellColumnSelectable(freecellColumn);
}

void Freecell::swapToFreecell(int swapColumn, int freecellColumn)
{
    Q_ASSERT(freecellColumn >= 0 && freecellColumn < freecell->stacksCount());
    Q_ASSERT(swapColumn >= 0 && swapColumn < swap->stacksCount());
    qDebug() << "swap to freecell" << swapColumn << freecellColumn;
    if (swap->stackCardsCount(swapColumn) == 0) return;

    const Card* swapCard = swap->cardList(swapColumn).last();
    if (freecell->stackCardsCount(freecellColumn) > 0) {
        if (!isFreecellStacking(freecell->cardList(freecellColumn).last(), swapCard)) return;
    }

    Card* swappingCard = swap->popCard(swapColumn);
    freecell->pushCard(freecellColumn, swappingCard);
}

void Freecell::swapToAces(int swapColumn, int acesColumn)
{
    Q_ASSERT(swapColumn >= 0 && swapColumn < swap->stacksCount());
    qDebug() << "swap to aces" << swapColumn << acesColumn;
    if (swap->stackCardsCount(swapColumn) == 0) return;

    const Card* swapCard = swap->cardList(swapColumn).last();

    if (acesColumn >= 0 && acesColumn < aces->stacksCount()) {
        if (cardToAces(swapCard, acesColumn))
            swap->popCard(swapColumn);
    }
    else if (acesColumn < 0) {
        for (int i = 0; i < aces->stacksCount(); i++) {
            if (cardToAces(swapCard, i)) {
                swap->popCard(swapColumn);
                break;
            }
        }
    }
}

bool Freecell::isFreecellStacking(const Card *bottom, const Card *top)
{
    return (top->getColor() != bottom->getColor()) &&
            (top->getValue() == bottom->getValue() - 1);
}

bool Freecell::isAcesStacking(const Card *bottom, const Card *top)
{
    return (top->getSuit() == bottom->getSuit()) &&
            (top->getValue() == bottom->getValue() + 1);
}

QList<const Card *> Freecell::movingCards(int stack)
{
    Q_ASSERT(stack >= 0 && stack < freecell->stacksCount());
    QList<const Card*> stackList = freecell->cardList(stack);
    QList<const Card*> movingList;

    while(!stackList.isEmpty()) {
        if (!stackList.last()->isFlipped()) break;
        if (!movingList.isEmpty()) {
            if (!isFreecellStacking(stackList.last(), movingList.first())) break;
        }
        movingList.prepend(stackList.takeLast());
    }

    return movingList;
}

bool Freecell::cardToAces(const Card *card, int column)
{
    Q_ASSERT(column >= 0 && column < aces->stacksCount());
    Q_ASSERT(card != 00);

    // Check bad conditions
    if (aces->stackCardsCount(column) == 0) {
        if (card->getValue() != Card::Ace) return false;
    }
    else {
        if (!isAcesStacking(aces->cardList(column).last(), card)) return false;
    }

    // Makes the move
    Card* nonconstcard = (Card*)card;
    nonconstcard->setSelectable(false);
    aces->pushCard(column, nonconstcard);
    return true;
}

void Freecell::setFreecellColumnSelectable(int freecellColumn)
{
    Q_ASSERT(freecellColumn >= 0 && freecellColumn < freecell->stacksCount());
    if (freecell->cardList(freecellColumn).count() > 0) {
        // Have to make all cards selectable from top to last movable card
        QList<const Card*> selectableList = movingCards(freecellColumn);
        for (int i = freecell->stackCardsCount(freecellColumn) - 1; i >= freecell->stackCardsCount(freecellColumn) - selectableList.count(); i--)
            freecell->setCardSelectable(freecellColumn, freecell->cardList(freecellColumn).at(i), true);
    }
}
