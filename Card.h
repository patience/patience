#ifndef CARD_H
#define CARD_H

#include <QObject>

class Card : public QObject
{
    Q_OBJECT
    Q_ENUMS(Color)
    Q_ENUMS(Suit)
    Q_ENUMS(Value)
    Q_PROPERTY(Color color READ getColor NOTIFY colorChanged)
    Q_PROPERTY(Suit suit READ getSuit NOTIFY suitChanged)
    Q_PROPERTY(Value value READ getValue NOTIFY valueChanged)
    Q_PROPERTY(bool flipped READ isFlipped NOTIFY flippedChanged)
    Q_PROPERTY(bool selectable READ isSelectable NOTIFY selectableChanged)

public:
    enum Color { Red, Black };
    enum Suit { Hearts = 0, Diamonds, Clubs, Spades };
    enum Value { Ace = 1, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King };

public:
    explicit Card(QObject *parent = 0);
    explicit Card(Suit s, Value v, bool isFlipped = false, QObject* parent = 0);

    Color getColor() const { return color; }
    Suit getSuit() const { return suit; }
    Value getValue() const { return value; }
    bool isFlipped() const { return flipped; }
    bool isSelectable() const { return selectable; }
    bool isValid() const { return value != 0; }

    void flip();
    void setFlipped(bool flip);
    void setSelectable(bool select);

signals:
    void colorChanged(Color);
    void suitChanged(Suit);
    void valueChanged(Value);
    void flippedChanged(bool);
    void selectableChanged(bool);

private:
    Color color;
    Suit suit;
    Value value;
    bool flipped;
    bool selectable;
};

#endif // CARD_H
