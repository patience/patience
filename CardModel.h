#ifndef CARDMODEL_H
#define CARDMODEL_H

#include <QAbstractItemModel>
class Card;

class CardModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum CardRoles {
        SuitRole = Qt::UserRole + 1,
        ValueRole,
        FlippedRole,
        SelectableRole,
        ColorRole,
        LastRole
    };

public:
    explicit CardModel(QObject *parent = 0);
    virtual ~CardModel();

protected:
    QVariant dataFromCard(const Card* card, int role) const;

private:
    void registerQMLroles();
};

#endif // CARDMODEL_H
