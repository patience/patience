#ifndef NLASTPROXYMODEL_H
#define NLASTPROXYMODEL_H

#include "CardModel.h"

class CardListModel;

class NLastProxyModel : public CardModel
{
    Q_OBJECT
public:
    explicit NLastProxyModel(int lastCount, QObject *parent = 0);
    void setSourceModel(CardListModel *sourceModel);

    QModelIndex index(int row, int column = 0, const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &child = QModelIndex()) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;

private slots:
    void onSourceAboutToBeModified();
    void onSourceModified();
    void onSourceDataChanged();

private:
    CardListModel *sourceModel;
    int lastCount;
};

#endif // NLASTPROXYMODEL_H
