#include "NLastProxyModel.h"

#include <QtDebug>
#include "CardListModel.h"

NLastProxyModel::NLastProxyModel(int lastCount, QObject *parent) :
    CardModel(parent),
    lastCount(lastCount)
{
}

void NLastProxyModel::setSourceModel(CardListModel *sourceModel) {
    this->sourceModel = sourceModel;

    connect(sourceModel, SIGNAL(rowsAboutToBeInserted(const QModelIndex&, int, int)),
            this, SLOT(onSourceAboutToBeModified()));
    connect(sourceModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)),
            this, SLOT(onSourceModified()));
    connect(sourceModel, SIGNAL(rowsAboutToBeRemoved(const QModelIndex&, int, int)),
            this, SLOT(onSourceAboutToBeModified()));
    connect(sourceModel, SIGNAL(rowsRemoved(const QModelIndex&, int, int)),
            this, SLOT(onSourceModified()));
    connect(sourceModel, SIGNAL(modelAboutToBeReset()), this, SLOT(onSourceAboutToBeModified()));
    connect(sourceModel, SIGNAL(modelReset()), this, SLOT(onSourceModified()));
    connect(sourceModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(onSourceDataChanged()));
}

QModelIndex NLastProxyModel::index(int row, int column, const QModelIndex &parent) const {
    if (parent.isValid()) return QModelIndex();
    if (row < 0 || row >= lastCount || column < 0 || column > 0) return QModelIndex();
    return this->createIndex(row, column);
}

// No parent
QModelIndex NLastProxyModel::parent(const QModelIndex &) const {
    return QModelIndex();
}

int NLastProxyModel::rowCount(const QModelIndex &index) const {
    if (index.isValid()) return 0;
    return qMin(lastCount, sourceModel->rowCount());
}

int NLastProxyModel::columnCount(const QModelIndex &index) const {
    if (index.isValid()) return 0;
    return 1;
}

QVariant NLastProxyModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) return QVariant();

    int sourceRow = sourceModel->rowCount() - rowCount() + index.row();
    sourceRow = qBound(0, sourceRow, sourceModel->rowCount() - 1);

    return sourceModel->data(sourceModel->index(sourceRow), role);
}

void NLastProxyModel::onSourceAboutToBeModified()
{
    beginResetModel();
}

void NLastProxyModel::onSourceModified()
{
    endResetModel();
}

void NLastProxyModel::onSourceDataChanged()
{
    emit dataChanged(index(0), index(rowCount() - 1));
}
