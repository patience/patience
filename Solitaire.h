#ifndef SOLITAIRE_H
#define SOLITAIRE_H

#include <QObject>

class QAbstractItemModel;
class NLastProxyModel;
class CardListModel;
class CardStacksModel;
class Card;

class Solitaire : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name NOTIFY nameChanged)

public:
    explicit Solitaire(QObject *parent = 0);
    QString name() const { return m_gameName; }

    Q_INVOKABLE CardStacksModel* solitaireModel() const { return m_solitaire; }
    Q_INVOKABLE CardStacksModel* acesModel() const { return m_aces; }
    Q_INVOKABLE CardListModel* deckModel() const { return m_deck; }
    Q_INVOKABLE QAbstractItemModel* discardModel() const;

signals:
    void nameChanged(const QString&);

public slots:
    void newGame();
    void drawCards();
    void solitaireMove(int startColumn, int endColumn);
    void discardToSolitaire(int column);
    void discardToAces(int column = -1);
    void solitaireToAces(int solitaireColumn, int acesColumn = -1);

private:
    QList<const Card*> movingCards(int stack);
    bool isSolitaireStacking(const Card* bottom, const Card* top);
    bool isAcesStacking(const Card* bottom, const Card* top);
    bool cardToAces(const Card* card, int column);
    void discardSetSelectability();

private:
    QString m_gameName;
    CardStacksModel* m_solitaire;              // 7 columns of solitaire's cards
    CardStacksModel* m_aces;                   // 4 columns of aces
    CardListModel* m_deck;                     // rest of the cards to draw
    CardListModel* m_discard;                  // cards drawn from the deck.
    NLastProxyModel* m_discardFiltered;        // Only give to GUI (whatever GUI it is!) the last 3 cards from the deck
};

#endif // SOLITAIRE_H
