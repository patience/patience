#include "Card.h"

Card::Card(QObject *parent) :
    QObject(parent),
    color(Red),
    suit(Hearts),
    value((Value)0),
    flipped(true),
    selectable(false)
{
}

Card::Card(Suit s, Value v, bool flipped, QObject *parent) :
    QObject(parent),
    suit(s),
    value(v),
    flipped(flipped),
    selectable(false)
{
    color = (s == Hearts || s == Diamonds) ? Red : Black;
}

void Card::flip() {
    flipped = !flipped;
    emit flippedChanged(flipped);
}

void Card::setFlipped(bool flip) {
    if (flipped != flip) {
        flipped = flip;
        emit flippedChanged(flip);
    }
}

void Card::setSelectable(bool select) {
    if (selectable != select) {
        selectable = select;
        emit selectableChanged(select);
    }
}
