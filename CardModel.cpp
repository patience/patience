#include "CardModel.h"

#include "Card.h"

CardModel::CardModel(QObject *parent) :
    QAbstractItemModel(parent)
{
    registerQMLroles();
}

CardModel::~CardModel() {

}

void CardModel::registerQMLroles()
{
    QHash<int, QByteArray> roles;
    roles[SuitRole] = "suit";
    roles[ValueRole] = "value";
    roles[ColorRole] = "color";
    roles[FlippedRole] = "flipped";
    roles[SelectableRole] = "selectable";
    setRoleNames(roles);
}

QVariant CardModel::dataFromCard(const Card *card, int role) const
{
    if (role == SuitRole) return card->getSuit();
    if (role == ValueRole) return card->getValue();
    if (role == ColorRole) return card->getColor();
    if (role == FlippedRole) return card->isFlipped();
    if (role == SelectableRole) return card->isSelectable();
    if (role == Qt::DisplayRole) return QVariant(QString("Card value %1 - Suit %2").arg(card->getValue()).arg(card->getSuit()));
    return QVariant();
}
