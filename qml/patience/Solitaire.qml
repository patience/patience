// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Patience 1.0

Item {
    id: root
    property int globalMargin: Math.max(10, width / 50)
    property int horizontalSpacing: width / 50
    property int cardsWidth: (root.width - 2 * root.globalMargin - 6 * root.horizontalSpacing) / 7
    property int cardsHeight: cardsWidth * 1.45
    property alias gameName: solitaireModel.name

    SolitaireModel {
        id: solitaireModel

        // Start a new game as soon as the component is created
        Component.onCompleted: newGame()
    }

    CardListView {
        id: deck
        cardListModel: solitaireModel.deckModel()
        horizontalSpacing: 0
        verticalSpacing: 0
        cardsWidth: root.cardsWidth
        cardsHeight: root.cardsHeight

        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: globalMargin
        anchors.topMargin: globalMargin

        MouseArea {
            anchors.fill: parent
            onClicked: { solitaireModel.drawCards(); }
        }
    }

    CardListView {
        id: discard
        cardListModel: solitaireModel.discardModel()
        horizontalSpacing: root.horizontalSpacing
        verticalSpacing: 0
        cardsWidth: root.cardsWidth
        cardsHeight: root.cardsHeight

        anchors.left: deck.right
        anchors.top: parent.top
        anchors.topMargin: globalMargin
        anchors.leftMargin: globalMargin
    }

    CardStackView {
        id: aces
        cardStackModel: solitaireModel.acesModel()
        horizontalSpacing: root.horizontalSpacing
        verticalSpacing: 0
        cardsWidth: root.cardsWidth
        cardsHeight: root.cardsHeight

        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: globalMargin
        anchors.rightMargin: globalMargin
    }

    CardStackView {
        id: solitaire
        cardStackModel: solitaireModel.solitaireModel()
        horizontalSpacing: root.horizontalSpacing
        verticalSpacing: root.cardsHeight / 7
        cardsWidth: root.cardsWidth
        cardsHeight: root.cardsHeight

        anchors.top: deck.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: globalMargin
    }
}
