// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Patience 1.0

Item {
    id: root
    property int globalMargin: Math.max(10, width / 50)
    property int horizontalSpacing: width / 50
    property int cardsWidth: (root.width - 2 * root.globalMargin - 7 * root.horizontalSpacing) / 8
    property int cardsHeight: cardsWidth * 1.45
    property alias gameName: freecellModel.name

    FreecellModel {
        id: freecellModel

        // Start a new game as soon as the component is created
        Component.onCompleted: newGame()
    }

    CardStackView {
        id: swap
        cardStackModel: freecellModel.swapModel()
        horizontalSpacing: root.horizontalSpacing
        verticalSpacing: 0
        cardsWidth: root.cardsWidth
        cardsHeight: root.cardsHeight

        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: globalMargin
        anchors.topMargin: globalMargin
    }

    CardStackView {
        id: aces
        cardStackModel: freecellModel.acesModel()
        horizontalSpacing: root.horizontalSpacing
        verticalSpacing: 0
        cardsWidth: root.cardsWidth
        cardsHeight: root.cardsHeight

        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: globalMargin
        anchors.rightMargin: globalMargin
    }

    CardStackView {
        id: freecell
        cardStackModel: freecellModel.freecellModel()
        horizontalSpacing: root.horizontalSpacing
        verticalSpacing: root.cardsHeight / 7
        cardsWidth: root.cardsWidth
        cardsHeight: root.cardsHeight

        anchors.top: swap.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: globalMargin
    }
}
