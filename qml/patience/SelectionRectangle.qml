// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: rootRectangle
    property alias gameName: textName.text
    property bool fullscreen: false
    signal clicked()

    color: "transparent"
    radius: 15
    border.color: "blue"
    border.width: fullscreen ? 0 : 5

    Text {
        id: textName
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 50
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 30
        color: "#f3eaea"
        opacity: fullscreen ? 0.0 : 0.3
    }

    MouseArea {
        enabled: !fullscreen
        hoverEnabled: !fullscreen
        anchors.fill: parent
        onHoveredChanged: containsMouse ? rootRectangle.border.color = "red" : rootRectangle.border.color = "blue"
        onClicked: rootRectangle.clicked()
    }
}
