// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: mainRectangle

    // Define gametext as a property refering to the text of the gameText object. This allow external setting of the name
    property alias gameName: gameText.text

    width: 600
    height: 400
    color: "#246612"

    Text {
        id: gameText
        color: "#f3eaea"
        text: "Game Name"
        font.pointSize: 50
        opacity: 0.3
        anchors.bottom: mainRectangle.bottom
        anchors.bottomMargin: 10
        anchors.horizontalCenter: mainRectangle.horizontalCenter
        font.pixelSize: Math.min(200, parent.width / text.length * 1.5)
        font.family: "Verdana"
    }
}
