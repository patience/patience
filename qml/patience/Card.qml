// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: card
    signal clicked()

    //Default size
    width: 100
    height: 145

    // Flipped indique que la carte est retournée (non visible)
    property bool flipped: true

    // Valeur initiales d'une carte : As de coeur
    property int suit: 0
    property int value: 1

    Flipable {
        id: flipable
        anchors.fill: parent

        // Note : the front is the face initialy displayed, so we put the 'back' there to have cards invisible by default
        front: Image {
            anchors.fill: parent
            source: "back.svg"
        }

        back: CardFace {
            anchors.fill: parent
            suit: card.suit
            value: card.value
        }

        states: State {
            name: "back"
            PropertyChanges { target: rotation; angle: 180 }
            when: !card.flipped
        }

        transform: Rotation {
            id: rotation
            origin.x: flipable.width/2
            origin.y: flipable.height/2
            axis.x: 0; axis.y: 1; axis.z: 0     // set axis.y to 1 to rotate around y-axis
            angle: 0                            // the default angle
        }

        transitions: Transition {
            NumberAnimation { target: rotation; property: "angle"; duration: 500 }
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: card.clicked()
    }
}
