import QtQuick 1.1

/**
  * CardListView dispose cards from the model, the final height and width of the component depends on the cards in list unless a fixed size
  * is fixed. Hints for cards design can be given with horizontalSpacing (verticalSpacing), cardsWidth and cardsHeight properties.
  *
  * When a card is clicked, double clicked or pressed and released, corresponding signals are emitted. an index of -1 means we clicked on the empty
  * stack. The empty stack is a rectangle drawn when no more cards are visible in the list model.
  */
Item {
    id: root
    property alias cardListModel: cardListDataModel.model
    property alias cardListModelIndex: cardListDataModel.rootIndex

    // Defaut values for design properties, we may adapt them from external component
    property int horizontalSpacing: 25
    property int verticalSpacing: 25
    property int cardsWidth: 100
    property int cardsHeight: 145

    // Make custom signals available out of this component
    signal cardClicked(int index, int suit, int value)
    signal cardDoubleClicked(int index, int suit, int value)
    signal cardPressed(int index)
    signal cardReleased(int x, int y)

    // This Component dimension are computed from the model count and cards size.
    width: cardsWidth + horizontalSpacing * Math.max(0, (cardListView.count - 1))
    height: cardsHeight + verticalSpacing * Math.max(0, (cardListView.count - 1))

    // Empty slot : draws an empty rectangle when there's no more cards in the list model
    Rectangle {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 2
        anchors.topMargin: 2
        width: cardsWidth - 4
        height: cardsHeight - 4
        radius: 5
        border.width: 3
        color: "transparent"
        border.color: "black"

        MouseArea {
            anchors.fill: parent
            onClicked: root.cardClicked(-1, -1, -1);
            onDoubleClicked: root.cardDoubleClicked(-1, -1, -1);
        }
    }

    // Card list : a repeater creates a Card component for each card in the model
    Repeater {
        id: cardListView
        model: VisualDataModel {
            id: cardListDataModel

            delegate: Card {
                id: thiscard
                opacity: (model.value) ? 1 : 0
                x: horizontalSpacing * index
                y: verticalSpacing * index
                width: cardsWidth;
                height: cardsHeight;
                value: (model.value) ? model.value : 0;
                suit: (model.suit) ? model.suit : 0;
                flipped: (model.flipped) ? model.flipped : 0;
                onClicked: root.cardClicked(index, suit, value)
            }
        }
    }
}
