// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

/*
 * Draws the visible face of a card based only on text and the 4 images of the 4 suits.
 * The image is drawn on the external oject dimension, which should be Card.qml
 * So all subitem are positionned based on width and height. It could by the way be CPU
 * expensive to resize a full card deck in real time.
 *
 * Text and card color are of course selected based on suit and value properties.
 */
Rectangle {
    id: faceRoot
    property int suit: 0
    property int value: 10
    width: 100
    height: 145

    color: "#ffffff"
    radius: 6
    border.width: 1
    smooth: true
    border.color: "#000000"

    function imageSuitPath(suit) {
        if (suit === 0) return "Hearts.png"
        if (suit === 1) return "Diamonds.png"
        if (suit === 2) return "Clubs.png"
        if (suit === 3) return "Spades.png"
    }

    function valueToText(value) {
        if (value === 1) return "As"
        if (value < 11) return value + '';
        if (value === 11) return 'V';
        if (value === 12) return 'D';
        if (value === 13) return 'R';
    }

    Component {
        id: cardFaceCorner

        Item {
            Text {
                id: cornerText
                text: valueToText(value)
                horizontalAlignment: Text.AlignHCenter
                color: if (suit < 2) "#ff0000"; else "#000000";
                smooth: true
                anchors.left: parent.left
                anchors.leftMargin: 4
                anchors.top: parent.top
                anchors.topMargin: 4
                font.bold: false
                font.family: "Verdana"
                font.pixelSize: faceRoot.width / 7
            }

            Image {
                id: cornerRightImage
                smooth: true
                width: Math.min(faceRoot.width, faceRoot.height) / 7
                height: Math.min(faceRoot.width, faceRoot.height) / 7
                anchors.left: cornerText.right
                anchors.leftMargin: 4
                anchors.verticalCenterOffset: 0
                anchors.verticalCenter: cornerText.verticalCenter
                fillMode: Image.PreserveAspectFit
                source: imageSuitPath(suit)
            }

            Image {
                id: cornerLowImage
                smooth: true
                width: Math.min(faceRoot.width, faceRoot.height) / 7
                height: Math.min(faceRoot.width, faceRoot.height) / 7
                anchors.horizontalCenter: cornerText.horizontalCenter
                anchors.leftMargin: 0
                anchors.top: cornerText.bottom
                anchors.topMargin: 4
                fillMode: Image.PreserveAspectFit
                source: imageSuitPath(suit)
            }

        }
    }

    // Top left card corner
    Loader {
        sourceComponent: cardFaceCorner
        anchors.top: faceRoot.top
        anchors.left: faceRoot.left
    }

    // Bottom right is the same, rotated half turn
    Loader {
        sourceComponent: cardFaceCorner
        rotation: 180
        anchors.bottom: faceRoot.bottom
        anchors.right: faceRoot.right
    }

    Text {
        id: centerText
        smooth: true
        color: if (suit < 2) "#ff0000"; else "#000000";
        text: valueToText(value)
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.verticalCenter
        anchors.bottomMargin: -2
        font.pixelSize: Math.min(faceRoot.width, faceRoot.height) / 3
        font.family: "Verdana"
        font.bold: false
    }

    Image {
        id: centerImage
        smooth: true
        width: faceRoot.width / 3
        height: faceRoot.width / 3
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.verticalCenter
        anchors.topMargin: -2
        source: imageSuitPath(suit)
        fillMode: Image.PreserveAspectFit
    }
}
