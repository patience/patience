// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: root
    property QtObject cardStackModel

    // Horizontal spacing will be used for inter column spacing
    property int horizontalSpacing: 25
    property int verticalSpacing: 25
    property int cardsWidth: 100
    property int cardsHeight: 145

    // Make custom signals available out of this component
    signal cardClicked(int stackIndex, int index, int suit, int value)
    signal cardDoubleClicked(int stackIndex, int index, int suit, int value)
    signal cardPressed(int column)
    signal cardReleased(int x, int y)

    // This Component dimension are computed from the model count and cards size.
    width: cardsWidth * stackRow.count + horizontalSpacing * Math.max(0, (stackRow.count - 1))
    height: cardsHeight

    Row {
        spacing: horizontalSpacing

        Repeater {
            id: stackRow
            model: VisualDataModel {
                id: visualModel
                model: cardStackModel
                delegate: CardListView {
                    horizontalSpacing: 0
                    verticalSpacing: root.verticalSpacing
                    cardsWidth: root.cardsWidth
                    cardsHeight: root.cardsHeight

                    cardListModelIndex: visualModel.modelIndex(index)
                    cardListModel: cardStackModel
                }
            }
        }
    }
}
