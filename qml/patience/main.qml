// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    id: root
    width: 900
    height: 600

    Board {
        id: board
        anchors.fill: parent
        gameName: "Choose your game"

        SelectionRectangle {
            id: solitaireRectangle
            gameName: solitaire.gameName
            width: root.width / 3
            height: root.width / 3
            anchors.top: parent.top
            anchors.topMargin: root.height / 6
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: -parent.width / 4

            onClicked: root.state = "Solitaire"
            Solitaire {
                id: solitaire
                anchors.fill: parent
            }
        }

        SelectionRectangle {
            id: freecellRectangle
            gameName: freecell.gameName
            width: root.width / 3
            height: root.width / 3
            anchors.top: parent.top
            anchors.topMargin: root.height / 6
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: parent.width / 4

            onClicked: root.state = "Freecell"
            Freecell {
                id: freecell
                anchors.fill: parent
            }
        }
    }

    states: [
        State {
            name: "Solitaire"
            AnchorChanges {
                target: solitaireRectangle
                anchors.horizontalCenter: undefined
                anchors.top: board.top
                anchors.left: board.left
                anchors.right: board.right
                anchors.bottom: board.bottom
            }
            PropertyChanges {
                target: solitaireRectangle
                anchors.topMargin: 0
                fullscreen: true
            }
            PropertyChanges {
                target: board
                gameName: solitaire.gameName
            }
            PropertyChanges {
                target: freecellRectangle
                opacity: 0
            }
        },
        State {
            name: "Freecell"
            AnchorChanges {
                target: freecellRectangle
                anchors.horizontalCenter: undefined
                anchors.top: board.top
                anchors.left: board.left
                anchors.right: board.right
                anchors.bottom: board.bottom
            }
            PropertyChanges {
                target: freecellRectangle
                anchors.topMargin: 0
                fullscreen: true
            }
            PropertyChanges {
                target: board
                gameName: freecell.gameName
            }
            PropertyChanges {
                target: solitaireRectangle
                opacity: 0
            }
        }
    ]

    transitions: Transition {
        AnchorAnimation { duration: 300; easing.type: Easing.InOutQuad }
        NumberAnimation { target: solitaireRectangle; property: "opacity"; duration: 200; easing.type: Easing.InOutQuad }
        NumberAnimation { target: freecellRectangle; property: "opacity"; duration: 200; easing.type: Easing.InOutQuad }
    }

    focus: true
    Keys.onPressed: {
        if (event.key == Qt.Key_Escape) root.state = ""
    }
}
