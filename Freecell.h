#ifndef FREECELL_H
#define FREECELL_H

#include <QObject>

class QDeclarativeContext;
class NLastProxyModel;
class CardStacksModel;
class Card;

class Freecell : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name NOTIFY nameChanged)

public:
    explicit Freecell(QObject *parent = 0);
    QString name() const { return tr("Freecell"); }

    Q_INVOKABLE CardStacksModel* freecellModel() const { return freecell; }
    Q_INVOKABLE CardStacksModel* acesModel() const { return aces; }
    Q_INVOKABLE CardStacksModel* swapModel() const { return swap; }

signals:
    void nameChanged(QString);

public slots:
    void newGame();
    void freecellMove(int startColumn, int endColumn);
    void freecellToAces(int freecellColumn, int acesColumn = -1);
    void freecellToSwap(int freecellColumn, int swapColumn = -1);
    void swapToFreecell(int swapColumn, int freecellColumn);
    void swapToAces(int swapColumn, int acesColumn = -1);

private:
    bool isFreecellStacking(const Card* bottom, const Card* top);
    bool isAcesStacking(const Card* bottom, const Card* top);
    QList<const Card*> movingCards(int stack);
    bool cardToAces(const Card* card, int column);
    void setFreecellColumnSelectable(int freecellColumn);

private:
    CardStacksModel* freecell;               // 8 columns of freecell's cards
    CardStacksModel* aces;                   // 4 columns of aces
    CardStacksModel* swap;                   // 4 columns of swap place
};

#endif // FREECELL_H
