#include "CardListModel.h"
#include "Card.h"

CardListModel::CardListModel(QObject *parent) :
    CardModel(parent)
{
}

CardListModel::~CardListModel() {
    clear();
}

void CardListModel::pushCard(Card *card) {
    if (card == 00) return;
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    this->cards << card;
    endInsertRows();
}

void CardListModel::pushCards(QList<Card *> cards) {
    if (cards.isEmpty()) return;
    beginInsertRows(QModelIndex(), rowCount(), rowCount() + cards.count() - 1);
    this->cards << cards;
    endInsertRows();
}

Card* CardListModel::popCard() {
    if (cards.isEmpty()) return 00;
    beginRemoveRows(QModelIndex(), rowCount() - 1, rowCount() - 1);
    Card* card = cards.takeLast();
    endRemoveRows();
    return card;
}

QList<Card*> CardListModel::popLasts(int count) {
    if (count <= 0) return QList<Card*>();

    QList<Card*> list;
    count = qMin(count, cards.count());
    beginRemoveRows(QModelIndex(), rowCount() - count, rowCount() - 1);
    while(count > 0 && cards.count() > 0) {
        list.prepend(cards.takeLast());
        count--;
    }
    endRemoveRows();
    return list;
}

QList<Card*> CardListModel::takeAll() {
    QList<Card*> list = cards;
    if (rowCount() < 1) return list;
    beginRemoveRows(QModelIndex(), 0, rowCount() - 1);
    cards.clear();
    endRemoveRows();
    return list;
}

int CardListModel::rowCount(const QModelIndex &index) const {
    if (index.isValid()) return 0;      // No tree structure here
    return cards.count();
}

int CardListModel::columnCount(const QModelIndex &) const {
    return 1;
}

// Nobody has a parent here
QModelIndex CardListModel::index(int row, int column, const QModelIndex &parent) const {
    if (!parent.isValid()) {
        if (row < 0 || row >= cards.count() || column != 0) return QModelIndex();
        return createIndex(row, column);
    }
    else {
        return QModelIndex();
    }
}

// Nobody has a parent here
QModelIndex CardListModel::parent(const QModelIndex &) const {
    return QModelIndex();
}

QVariant CardListModel::data(const QModelIndex &index, int role) const {
    if (index.row() < 0 || index.row() > cards.count())
        return QVariant();

    const Card *card = cards[index.row()];
    return dataFromCard(card, role);
}

void CardListModel::clear() {
    beginResetModel();
    foreach(Card* card, cards) card->deleteLater();
    cards.clear();
    endResetModel();
}

void CardListModel::buildDeck32() {
    beginResetModel();

    // First clear previous cards
    foreach(Card* card, cards) card->deleteLater();
    cards.clear();

    // Then fill with the deck cards data
    for (int s = 0; s < 4; s++) {
        for (int i = 7; i < 14; i++) cards.append(new Card(Card::Suit(s), Card::Value(i), true));
        cards.append(new Card(Card::Suit(s), Card::Ace, true));
    }

    endResetModel();
}

void CardListModel::buildDeck52() {
    beginResetModel();

    // First clear previous cards
    foreach(Card* card, cards) card->deleteLater();
    cards.clear();

    // Then fill with the deck cards data
    for (int s = 0; s < 4; s++) {
        for (int i = 1; i < 14; i++) cards.append(new Card(Card::Suit(s), Card::Value(i), true));
    }

    endResetModel();
}

void CardListModel::shuffle() {
    beginResetModel();
    for ( int i = cards.size(); i > 1; --i )
        cards.swap( i - 1, qrand() % i );
    endResetModel();
}

bool CardListModel::contains(int suit, int value) const {
    for (int i = 0; i < cards.count(); ++i) {
        if (cards[i]->getSuit() == suit && cards[i]->getValue() == value) return true;
    }
    return false;
}

void CardListModel::setCardSelectable(const Card *card, bool select) {
    setCardSelectable(card->getSuit(), card->getValue(), select);
}

void CardListModel::setCardSelectable(int suit, int value, bool select) {
    for(int i = 0; i < cards.count(); i++) {
        if (cards[i]->getSuit() == suit &&
                cards[i]->getValue() == value &&
                cards[i]->isSelectable() != select) {
            cards[i]->setSelectable(select);
            emit dataChanged(index(i), index(i));
            return;
        }
    }
}

void CardListModel::setCardFlipped(const Card *card, bool flipped) {
    setCardFlipped(card->getSuit(), card->getValue(), flipped);
}

void CardListModel::setCardFlipped(int suit, int value, bool flipped) {
    for(int i = 0; i < cards.count(); i++) {
        if (cards[i]->getSuit() == suit &&
                cards[i]->getValue() == value &&
                cards[i]->isFlipped() != flipped) {
            cards[i]->setFlipped(flipped);
            emit dataChanged(index(i), index(i));
            return;
        }
    }
}

const Card * CardListModel::getCard(int suit, int value) const
{
    foreach(Card* card, cards) {
        if (card->getSuit() == suit && card->getValue() == value) return card;
    }

    return 0;
}

void CardListModel::setAllFlipped(bool flipped)
{
    foreach(Card* card, cards) card->setFlipped(flipped);
    emit dataChanged(index(0), index(rowCount() - 1));
}

void CardListModel::setAllSelectable(bool selectable)
{
    foreach(Card* card, cards) card->setSelectable(selectable);
    emit dataChanged(index(0), index(rowCount() - 1));
}

bool CardListModel::isEmpty() const
{
    return cards.size() == 0;
}

QList<const Card *> CardListModel::cardList() const
{
    QList<const Card* > constList;
    foreach(Card* card, cards) {
        constList.append(card);
    }
    return constList;
}
