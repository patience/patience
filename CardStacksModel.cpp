#include "CardStacksModel.h"
#include "Card.h"

#include <QtDebug>

CardStacksModel::CardStacksModel(int columnCount, QObject *parent) :
    CardModel(parent)
{
    for (int i = 0; i < columnCount; i++) cards.append(QList<Card*>());
}

CardStacksModel::~CardStacksModel() {
    clearAllStacks();
}

Qt::ItemFlags CardStacksModel::flags(const QModelIndex &index) const {
    if (!index.isValid()) return QAbstractItemModel::flags(index);
    return Qt::ItemIsEnabled;
}

QVariant CardStacksModel::data(const QModelIndex &index, int role) const {
    // Easier since we stored cards pointers into the index of course.
    const Card* card = (const Card*)index.internalPointer();
    if (card == 00) return QVariant();

    return dataFromCard(card, role);
}

int CardStacksModel::rowCount(const QModelIndex &parent) const {
    if (!parent.isValid()) return cards.count();
    if (parent.internalPointer() == 00) {
        int stackIndex = parent.row();
        int count = 0;
        if (stackIndex >= 0 && stackIndex < cards.count()) count = cards.value(stackIndex).count();
        return count;
    }
    else return 0;
}

int CardStacksModel::columnCount(const QModelIndex&) const {
    return 1;
}

void CardStacksModel::pushCard(int stackIndex, Card *card) {
    if (card == 00) return;
    if (stackIndex < 0 || stackIndex >= cards.count()) return;
    QList<Card*>& cardStack = cards[stackIndex];

    beginInsertRows(index(stackIndex, 0), cardStack.count(), cardStack.count());
    cardStack.append(card);
    endInsertRows();
}

void CardStacksModel::pushCards(int stackIndex, QList<Card *> cardList) {
    if (stackIndex < 0 || stackIndex >= cards.count()) return;
    QList<Card*>& cardStack = cards[stackIndex];

    beginInsertRows(index(stackIndex, 0), cardStack.count(), cardStack.count() + cardList.count() - 1);
    cardStack.append(cardList);
    endInsertRows();
}

Card* CardStacksModel::popCard(int stackIndex) {
    if (stackIndex < 0 || stackIndex >= cards.count()) return 00;
    QList<Card*>& cardStack = cards[stackIndex];
    if (cardStack.isEmpty()) return 00;
    beginRemoveRows(index(stackIndex, 0), cardStack.count() - 1, cardStack.count() - 1);
    Card* card = cardStack.takeLast();
    endRemoveRows();
    return card;
}

QList<Card*> CardStacksModel::popLasts(int stackIndex, int popCount) {
    QList<Card*> list;
    if (stackIndex < 0 || stackIndex >= cards.count()) return list;
    if (popCount <= 0) return list;
    QList<Card*>& cardStack = cards[stackIndex];
    popCount = qMin(popCount, cardStack.count());
    beginRemoveRows(index(stackIndex, 0), cardStack.count() - popCount, cardStack.count() - 1);
    while(popCount > 0 && cardStack.count() > 0) {
        list.prepend(cardStack.takeLast());
        popCount--;
    }
    endRemoveRows();
    return list;
}

QList<Card*> CardStacksModel::takeAll(int stackIndex) {
    if (stackIndex < 0 || stackIndex >= cards.count()) return QList<Card*>();
    QList<Card*> list(cards[stackIndex]);
    beginRemoveRows(index(stackIndex, 0), 0, cards[stackIndex].count() - 1);
    cards[stackIndex].clear();
    endRemoveRows();
    return list;
}

// Removes all cards from columns, do not change the column count
void CardStacksModel::clearAllStacks() {
    beginResetModel();
    for (int i = 0; i < cards.count(); i++) {
        foreach(Card* card, cards[i]) card->deleteLater();
        cards[i].clear();
    }
    endResetModel();
}

void CardStacksModel::clearStack(int stackIndex) {
    if (stackIndex < 0 || stackIndex >= cards.count()) return;
    QList<Card*>& cardStack = cards[stackIndex];
    beginRemoveRows(index(stackIndex, 0), 0, cards[stackIndex].count() - 1);
    foreach(Card* card, cardStack) card->deleteLater();
    cards[stackIndex].clear();
    endRemoveRows();
}


// We will embed the Card* pointer with all Model indexes except top level ones
QModelIndex CardStacksModel::index(int row, int column, const QModelIndex &parent) const {
    if (!parent.isValid()) {
        if (row < 0 || row >= cards.count() || column != 0) return QModelIndex();
        return createIndex(row, column, 00);        // Top level item can't have a card data pointer !
    }
    else {
        if (parent.row() < 0 || parent.row() >= cards.count() || parent.column() != 0) return QModelIndex();
        QList<Card*> cardStack = cards.value(parent.row());

        return createIndex(row, column, cardStack.value(row));
    }
}

QModelIndex CardStacksModel::parent(const QModelIndex &child) const {
    if (!child.isValid()) return QModelIndex();

    // Top level if the pointer is null, top level dont have parents
    if (child.internalPointer() == 00) return QModelIndex();

    // Search for the column owner of this card.
    Card* indexedCard = (Card*)child.internalPointer();
    int stack;
    for (stack = 0; stack < stacksCount(); stack++) {
        if (cards[stack].contains(indexedCard)) break;
    }
    if (stack == stacksCount()) return QModelIndex();

    return createIndex(stack, 0, 00);
}

void CardStacksModel::dumpModelData() const {
    qDebug() << "Model number of rows" << cards.count();
    for(int i = 0; i < cards.count(); i++) {
        QList<Card*> stack = cards.at(i);
        qDebug() << "Listing cards in stack " << i << stack.count();
        for(int c = 0; c < stack.count(); c++) {
            Card* card = stack.at(c);
            qDebug() << "    card " << c << " : value - suit" << card->getValue() << card->getSuit();
        }
    }
}

bool CardStacksModel::isEmpty(int stackIndex) const {
    if (stackIndex < 0 || stackIndex >= cards.count()) return false;
    return cards[stackIndex].isEmpty();
}

int CardStacksModel::emptyStacksCount() const {
    int emptycount = 0;
    for(int i = 0; i < cards.count(); i++) {
        if (cards[i].isEmpty()) emptycount++;
    }

    return emptycount;
}

int CardStacksModel::stackCardsCount(int stackIndex) const {
    if (stackIndex < 0 || stackIndex >= cards.count()) return 0;
    return cards[stackIndex].count();
}

bool CardStacksModel::contains(int stackIndex, int suit, int value) const {
    if (stackIndex < 0 || stackIndex >= cards.count()) return false;
    const QList<Card*>& cardStack = cards[stackIndex];
    for (int i = 0; i < cardStack.count(); ++i) {
        if (cardStack[i]->getSuit() == suit && cardStack[i]->getValue() == value) return true;
    }
    return false;
}

const Card * CardStacksModel::getCard(int stackIndex, int suit, int value) const {
    if (stackIndex < 0 || stackIndex >= cards.count()) return 00;
    const QList<Card*>& cardStack = cards[stackIndex];
    for (int i = 0; i < cardStack.count(); ++i) {
        if (cardStack[i]->getSuit() == suit && cardStack[i]->getValue() == value) return  cardStack[i];
    }
    return 00;
}

QList<const Card*> CardStacksModel::cardList(int stackIndex) const {
    if (stackIndex < 0 || stackIndex >= cards.count()) return QList<const Card*>();
    QList<const Card* > constList;
    foreach(Card* card, cards[stackIndex]) {
        constList.append(card);
    }
    return constList;
}

void CardStacksModel::setCardSelectable(int stackIndex, const Card* card, bool select) {
    setCardSelectable(stackIndex, card->getSuit(), card->getValue(), select);
}

void CardStacksModel::setCardSelectable(int stackIndex, int suit, int value, bool select) {
    if (stackIndex < 0 || stackIndex >= cards.count()) return;
    const QList<Card*>& cardStack = cards[stackIndex];
    for(int i = 0; i < cardStack.count(); i++) {
        if (cardStack[i]->getSuit() == suit &&
                cardStack[i]->getValue() == value &&
                cardStack[i]->isSelectable() != select) {
            cardStack[i]->setSelectable(select);
            emit dataChanged(index(i, 0, index(stackIndex, 0)), index(i, 0, index(stackIndex, 0)));
            return;
        }
    }
}

void CardStacksModel::setCardFlipped(int stackIndex, const Card* card, bool flipped) {
    setCardFlipped(stackIndex, card->getSuit(), card->getValue(), flipped);
}

void CardStacksModel::setCardFlipped(int stackIndex, int suit, int value, bool flipped) {
    if (stackIndex < 0 || stackIndex >= cards.count()) return;
    const QList<Card*>& cardStack = cards[stackIndex];
    for(int i = 0; i < cardStack.count(); i++) {
        if (cardStack[i]->getSuit() == suit &&
                cardStack[i]->getValue() == value &&
                cardStack[i]->isFlipped() != flipped) {
            cardStack[i]->setFlipped(flipped);
            emit dataChanged(index(i, 0, index(stackIndex, 0)), index(i, 0, index(stackIndex, 0)));
            return;
        }
    }
}

void CardStacksModel::setAllFlipped(int stackIndex, bool flipped)
{
    if (stackIndex < 0 || stackIndex >= cards.count()) return;
    const QList<Card*>& cardStack = cards[stackIndex];
    foreach(Card* card, cardStack) card->setFlipped(flipped);
    emit dataChanged(index(0, 0, index(stackIndex, 0)), index(cardStack.count() - 1, 0, index(stackIndex, 0)));
}

void CardStacksModel::setAllSelectable(int stackIndex, bool selectable)
{
    if (stackIndex < 0 || stackIndex >= cards.count()) return;
    const QList<Card*>& cardStack = cards[stackIndex];
    foreach(Card* card, cardStack) card->setSelectable(selectable);
    emit dataChanged(index(0, 0, index(stackIndex, 0)), index(cardStack.count() - 1, 0, index(stackIndex, 0)));
}


