#ifndef CARDLISTMODEL_H
#define CARDLISTMODEL_H

#include <QObject>
#include "CardModel.h"

class Card;

class CardListModel : public CardModel
{
    Q_OBJECT

public:
    explicit CardListModel(QObject *parent = 0);
    virtual ~CardListModel();

    // Manipulation of the model
    void pushCard(Card* card);
    void pushCards(QList<Card*> cards);
    Card* popCard();
    QList<Card*> popLasts(int count);
    QList<Card*> takeAll();
    void clear();

    // Special for lists
    void buildDeck32();
    void buildDeck52();
    void shuffle();

    // Model info
    bool contains(int suit, int value) const;
    const Card* getCard(int suit, int value) const;
    bool isEmpty() const;
    QList<const Card*> cardList() const;

    void setCardSelectable(const Card* card, bool select);
    void setCardSelectable(int suit, int value, bool select);
    void setCardFlipped(const Card* card, bool flipped);
    void setCardFlipped(int suit, int value, bool flipped);
    void setAllFlipped(bool flipped);
    void setAllSelectable(bool selectable);

    // Abstract model
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QModelIndex index(int row, int column = 0, const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &child) const;

private:
    QList<Card*> cards;
};

#endif // CARDLISTMODEL_H
