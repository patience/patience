#include <QtGui/QApplication>
#include <QtDeclarative>
#include "qmlapplicationviewer.h"
#include "Solitaire.h"
#include "Freecell.h"
#include "CardListModel.h"
#include "CardStacksModel.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    // Registered to be able to create Solitaire instances from QML
    qmlRegisterType<Solitaire>("Patience", 1, 0, "SolitaireModel");
    qmlRegisterType<Freecell>("Patience", 1, 0, "FreecellModel");

    // Registered to be able to be used from QML
    qmlRegisterType<CardListModel>();
    qmlRegisterType<QAbstractItemModel>();
    qmlRegisterType<CardStacksModel>();

    // Initialize random seed
    qsrand(QDateTime::currentDateTime().toTime_t());

    // Launch qml app viewer and display main.qml file
    QmlApplicationViewer viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/patience/main.qml"));
    viewer.showExpanded();

    return app->exec();
}
