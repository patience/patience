#include "Solitaire.h"

#include <QtDebug>

#include "Card.h"
#include "CardListModel.h"
#include "CardStacksModel.h"
#include "NLastProxyModel.h"

Solitaire::Solitaire(QObject *parent) :
    QObject(parent)
{
    m_gameName = tr("Solitaire");
    m_solitaire = new CardStacksModel(7, this);
    m_aces = new CardStacksModel(4, this);
    m_deck = new CardListModel(this);
    m_discard = new CardListModel(this);

    m_discardFiltered = new NLastProxyModel(3, this);
    m_discardFiltered->setSourceModel(m_discard);
}

QAbstractItemModel* Solitaire::discardModel() const {
    return m_discardFiltered;
}

void Solitaire::newGame() {
    // Clear all cards from the game
    m_solitaire->clearAllStacks();
    m_aces->clearAllStacks();
    m_deck->clear();
    m_discard->clear();

    // Build new deck
    m_deck->buildDeck52();

    // Shuffle the deck
    m_deck->shuffle();

    // Distribute cards to the solitaire
    for (int i = 0; i < m_solitaire->stacksCount(); ++i) {
        for (int j = i; j < m_solitaire->stacksCount(); j++) {
            Card* card = m_deck->popCard();
            if (card == 00) {
                qWarning() << "Not enough cards in the deck to complete distribution";
                return;
            }
            if (i == j) {
                card->flip();
                card->setSelectable(true);
            }
            m_solitaire->pushCard(j, card);
        }
    }

}

void Solitaire::drawCards() {
    // If deck is empty, draw back the discard into the deck
    if (m_deck->rowCount() == 0) {
        QList<Card*> list = m_discard->takeAll();
        QList<Card*> reverseList;
        while (!list.isEmpty()) {
            Card* card = list.takeLast();
            card->setFlipped(true);
            reverseList.append(card);
        }
        m_deck->pushCards(reverseList);
        return;
    }

    // Draw up to three cards
    QList<Card*> cards = m_deck->popLasts(3);
    QList<Card*> reverseCards;
    for (int i = 0; i < cards.count(); i++) {
        cards[i]->flip();
        reverseCards.prepend(cards[i]);
    }
    m_discard->pushCards(reverseCards);

    discardSetSelectability();
}

void Solitaire::discardSetSelectability() {
    // Ensure only last card is selectable
    m_discard->setAllSelectable(false);
    if (m_discard->rowCount() > 0)
        m_discard->setCardSelectable(m_discard->cardList().last(), true);
}

//flipped == visible
void Solitaire::solitaireMove(int startColumn, int endColumn)
{
    Q_ASSERT(startColumn >= 0 && startColumn < m_solitaire->stacksCount());
    Q_ASSERT(endColumn >= 0 && endColumn < m_solitaire->stacksCount());
    if (startColumn == endColumn) return;

    // Get cards that may moves
    QList<const Card*> movingList = movingCards(startColumn);
    if (movingList.isEmpty()) return;

    // Check if the whole is able to move
    if (m_solitaire->stackCardsCount(endColumn) == 0) {
        if (movingList.first()->getValue() != Card::King) return;
    }
    else {
        while (!movingList.isEmpty()) {
            if (isSolitaireStacking(m_solitaire->cardList(endColumn).last(), movingList.first())) break;
            else movingList.removeFirst();
        }
    }
    if (movingList.isEmpty()) return;

    // Makes real move
    QList<Card*> poppedCards = m_solitaire->popLasts(startColumn, movingList.count());
    m_solitaire->pushCards(endColumn, poppedCards);
    if (m_solitaire->cardList(startColumn).count() > 0) {
        m_solitaire->setCardFlipped(startColumn, m_solitaire->cardList(startColumn).last(), false);
        m_solitaire->setCardSelectable(startColumn, m_solitaire->cardList(startColumn).last(), true);
    }
}

QList<const Card *> Solitaire::movingCards(int stack)
{
    Q_ASSERT(stack >= 0 && stack < m_solitaire->stacksCount());
    QList<const Card*> stackList = m_solitaire->cardList(stack);
    QList<const Card*> movingList;

    while(!stackList.isEmpty()) {
        if (stackList.last()->isFlipped()) break;
        if (!movingList.isEmpty()) {
            if (!isSolitaireStacking(stackList.last(), movingList.first())) break;
        }
        movingList.prepend(stackList.takeLast());
    }

    return movingList;
}

void Solitaire::discardToSolitaire(int column)
{
    if (m_discard->rowCount() == 0) return;
    const Card* lastDiscard = m_discard->cardList().last();

    if (m_solitaire->stackCardsCount(column) == 0) {
        if (lastDiscard->getValue() != Card::King) return;
    }
    else {
        if (!isSolitaireStacking(m_solitaire->cardList(column).last(), lastDiscard)) return;
    }

    m_solitaire->pushCard(column, m_discard->popCard());
    discardSetSelectability();
}

void Solitaire::discardToAces(int column)
{
    if (m_discard->rowCount() == 0) return;
    const Card* lastDiscard = m_discard->cardList().last();

    if (column >= 0 && column < m_aces->stacksCount()) {
        if (cardToAces(lastDiscard, column)) {
            m_discard->popCard();
            discardSetSelectability();
        }
    }
    else if (column < 0) {
        for (int i = 0; i < m_aces->stacksCount(); i++) {
            if (cardToAces(lastDiscard, i)) {
                m_discard->popCard();
                discardSetSelectability();
                break;
            }
        }
    }
}

void Solitaire::solitaireToAces(int solitaireColumn, int acesColumn)
{
    Q_ASSERT(solitaireColumn >= 0 && solitaireColumn < m_solitaire->stacksCount());
    if (m_solitaire->stackCardsCount(solitaireColumn) == 0) return;
    const Card* lastSolitaire = m_solitaire->cardList(solitaireColumn).last();

    if (acesColumn >= 0 && acesColumn < m_aces->stacksCount()) {
        if (cardToAces(lastSolitaire, acesColumn))
            m_solitaire->popCard(solitaireColumn);
    }
    else if (acesColumn < 0) {
        for (int i = 0; i < m_aces->stacksCount(); i++) {
            if (cardToAces(lastSolitaire, i)) {
                m_solitaire->popCard(solitaireColumn);
                break;
            }
        }
    }

    // Ensure last card is flipped true AND selectable
    if (m_solitaire->cardList(solitaireColumn).count() > 0) {
        m_solitaire->setCardFlipped(solitaireColumn, m_solitaire->cardList(solitaireColumn).last(), false);
        m_solitaire->setCardSelectable(solitaireColumn, m_solitaire->cardList(solitaireColumn).last(), true);
    }
}

// Try to append card to aces, return true if succes
bool Solitaire::cardToAces(const Card *card, int column)
{
    Q_ASSERT(column >= 0 && column < m_aces->stacksCount());
    Q_ASSERT(card != 00);

    // Check bad conditions
    if (m_aces->stackCardsCount(column) == 0) {
        if (card->getValue() != Card::Ace) return false;
    }
    else {
        if (!isAcesStacking(m_aces->cardList(column).last(), card)) return false;
    }

    // Makes the move
    Card* nonconstcard = (Card*)card;
    nonconstcard->setSelectable(false);
    m_aces->pushCard(column, nonconstcard);
    return true;
}

// Card should be the other color and inferior
bool Solitaire::isSolitaireStacking(const Card *bottom, const Card *top)
{
    return (top->getColor() != bottom->getColor()) &&
            (top->getValue() == bottom->getValue() - 1);
}

bool Solitaire::isAcesStacking(const Card *bottom, const Card *top)
{
    return (top->getSuit() == bottom->getSuit()) &&
            (top->getValue() == bottom->getValue() + 1);
}

